## Threat Detection

Inspired by the terminator's point of view scenes in the movies
like this [video](https://www.youtube.com/watch?v=zzcdPA6qYAU)  
and the [blog](https://engineering.matterport.com/splash-of-color-instance-segmentation-with-mask-r-cnn-and-tensorflow-7c761e238b46)  
using the [MASK_RCNN](https://github.com/matterport/Mask_RCNN) repo as a starting point  

In the first clip of the video the terminator is searching for clothes to take and put on. He runs analysis on possible size matches in real time.

Which made me think about having a real time visual threat analysis, potentially judging things like `immediacy of threat` or `magnitude of threat`  
For example `is the gun pointed toward or away from me` and `calibre of gun`


### Part one
#### Train the `mask_rcnn_coco` model to detect guns in images.

The model was trained on google cloud platform using a n1-highmem-8 with a Tesla P100 in about 30 minutes.  
Run from the project root to train the model : `python3 guns.py train --dataset=./datasets/guns --weights=coco`  
With a training set size of 62 and a validation set of 13

This model has a mAP:  0.65

[inspect_guns_data.ipynb](https://gitlab.com/HouseItMade/visual-threat-detection/blob/master/inspect_guns_data.ipynb) shows examples from the data sets and preprocessing steps  
[guns_detection.ipynb](https://gitlab.com/HouseItMade/visual-threat-detection/blob/master/guns_detection.ipynb) works through making predictions from the validation set, scoring the model, then making predictions on custom images

[link to the data set and model](https://drive.google.com/drive/folders/1kytvfMiRFCIpvY8qEEqHQTiiGZ4moQCr?usp=sharing)

google drive files can be easily downloaded from the terminal with

```
pip3 install gdown

# for the datasets (goes in /project_root/src/dastasets/)
gdown https://drive.google.com/uc?id=1BqLuP8xCJD4n5U8jnjfS65MDo3-QLbN-


```


### Part Two
#### Deploy the guns model to a webserver.

~~The guns model is now hosted on a digital ocean server (1vcpu @ 2.20GHz, 3 gig ram)  
and can be accessed at : http://167.99.97.205~~ (Not active as of August 14th 2021)



#### Next steps:
* train model on more classes
* have the model learn the difference between small and large calibre guns
* identifying the number of targets / types of targets
* assigning each target a threat score

#### Current Deficiencies :
* the guns dataset is small with 62 training and 13 validation images (try data augmentation and view metrics before and after)
* there are several types of guns in the dataset (eg handguns, rifles, shotguns and mounted guns)
* data set has a fairly restricted range of angles of guns
* inconsistent annotations
    - hands are sometimes included in masking region
    - sometimes includes scopes/attachments in region, sometimes does not
* how urls are stored and passed is ramshackle and needs to be improved
* prediction time on the digital ocean server is very slow (potentially more than 2 minutes)
* the digital ocean server doesnt appear to dump ram after running (ram usage sits at 80%)
