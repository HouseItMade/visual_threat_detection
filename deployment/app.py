import io
import os
import sys
import numpy as np
import flask
import skimage
import urllib.request
from flask import request, render_template
from wtforms import Form
import matplotlib
import matplotlib.pyplot as plt

import logging

filename = os.path.join(os.path.dirname(os.path.realpath(__file__)), 'app.log')
print("logs should be saved here ", filename)
logging.basicConfig(filename=filename, filemode='w', format='%(name)s - %(levelname)s - %(message)s', level=logging.DEBUG)

print(os.listdir())

logging.debug('starting up debugging logging')
logging.debug('saving log to {}'.format(filename))

# Import Mask RCNN
# Root directory of the project
# ROOT_DIR = os.path.abspath("./deployment/")
ROOT_DIR = os.path.abspath(".")
sys.path.append(ROOT_DIR)  # To find local version of the library
from deployment.utils import create_paths, read_paths

from mrcnn.config import Config
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize
from mrcnn.model import log


app = flask.Flask(__name__,
                    static_url_path="/deployment/static"
                    )
app.config.from_object(__name__)

def index():
    return flask.render_template("index.html")

class ReusableForm(Form):

    @app.route("/", methods=["GET"])
    def serve_form():
        return render_template("/form.html")

    @app.route("/", methods=['POST'])
    def result():
        # url to download image from
        url = request.form["url"]
        # set up paths each time an image is given
        logging.debug('got the url as a string')
        create_paths()
        DOWNLOADED_PATH, PREDICTED_PATH = read_paths()

        # needed to prevent error when reloading, or going back
        from keras import backend as K
        K.clear_session()


        if request.method =='POST':
            logging.debug('loading model')
            model = load_model()
            logging.debug('model loaded')
            url = str(request.form["url"])
            logging.debug('asking url for image')
            urllib.request.urlretrieve(url, DOWNLOADED_PATH)
            logging.debug('saved url image')
            image = skimage.io.imread(DOWNLOADED_PATH)
            logging.debug('read image with skimage')

            # Run detection
            logging.debug('predicting on image')
            results = model.detect([image], verbose=2)
            logging.debug('prediction complete')

            # save results
            r = results[0]
            logging.debug('saving image')
            visualize.save_predicted_image(image, r['rois'], r['masks'], r['class_ids'], 
                                        ["BG", "guns"], r['scores'], save_location=PREDICTED_PATH)
            logging.debug('image saved successfully')
            return render_template("result.html", url=url, filename=PREDICTED_PATH)

    # Clean up after saving image from url and displaying predicted image
    @app.after_request
    def remove_file(response):
        logging.debug('cleaning up files')
        _, PREDICTED_PATH = read_paths()
        try:
            # remove the previously saved predicted images
            old_files = ["./deployment/static/{}".format(x) for x in os.listdir("./deployment/static/")]
            for old_file in old_files:
                if old_file != "./deployment/static/place_holder.txt" and old_file != PREDICTED_PATH:
                    os.remove(old_file)
        except Exception as error:
            print("Error removed predicted file(s)", error)
        logging.debug('clean up complete')
        return response


def load_model():

    from src.guns import GunsConfig101

    class InferenceConfig(GunsConfig101):
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1

    inference_config = InferenceConfig()

    MODEL_DIR = os.path.join(ROOT_DIR, "logs")
    model_path = ROOT_DIR + "/src/mask_rcnn_guns.h5"
    # Recreate the model in inference mode
    model = modellib.MaskRCNN(mode="inference", 
                            config=inference_config,
                            model_dir=MODEL_DIR)

    print(model.config.display())


    # Load trained weights
    print("Loading weights from ", model_path)
    model.load_weights(model_path, by_name=True)

    return model


if __name__ == "__main__":
    port = int(os.environ.get("PORT", 8000))
    app.run(host="0.0.0.0", port=port)
    #  app.run()
